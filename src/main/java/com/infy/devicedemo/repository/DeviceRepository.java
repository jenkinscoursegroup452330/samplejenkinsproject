package com.infy.devicedemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.infy.devicedemo.entity.Devicedetails;
@Repository
public interface DeviceRepository extends JpaRepository<Devicedetails,String>{

}
