package com.infy.devicedemo.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.infy.devicedemo.entity.Devicedetails;
@Component
public class Devicedetailsdto {
	
	private String deviceId;
	private String deviceName;
	private String description;
	private int availabledevices;
	private int amount;
	private boolean deviceStatus;
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getAvailabledevices() {
		return availabledevices;
	}
	public void setAvailabledevices(int availabledevices) {
		this.availabledevices = availabledevices;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public boolean isDeviceStatus() {
		return deviceStatus;
	}
	public void setDeviceStatus(boolean deviceStatus) {
		this.deviceStatus = deviceStatus;
	}
	
	public Devicedetails convertIntoEntity(Devicedetailsdto dto) {
		Devicedetails device=new Devicedetails();
		
		device.setAmount(dto.getAmount());
		device.setAvailabledevices(dto.getAvailabledevices());
		device.setDescription(dto.getDescription());
		device.setDeviceId(dto.getDeviceId());
		device.setDeviceName(dto.getDeviceName());
		device.setDeviceStatus(dto.isDeviceStatus());
		return device;
	}
	
	public Devicedetailsdto convertIntoDto(Devicedetails device) {
		Devicedetailsdto dto= new Devicedetailsdto();
		dto.setAmount(device.getAmount());
		dto.setAvailabledevices(device.getAvailabledevices());
		dto.setDescription(device.getDescription());
		dto.setDeviceId(device.getDeviceId());
		dto.setDeviceName(device.getDeviceName());
		dto.setDeviceStatus(device.isDeviceStatus());
		return dto;
	}
	public List<Devicedetailsdto> createdtolist(List<Devicedetails> device){
		return device.stream().map(device1->convertIntoDto(device1)).collect(Collectors.toList());
	}
	public List<Devicedetails> createentitylist(List<Devicedetailsdto> devicedto){
		return devicedto.stream().map(dto->convertIntoEntity(dto)).collect(Collectors.toList());
	}
	
}
