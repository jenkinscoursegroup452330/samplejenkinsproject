package com.infy.devicedemo.service;

import java.util.List;

import com.infy.devicedemo.dto.Devicedetailsdto;

public interface Deviceservice {

	public Devicedetailsdto saveorder(Devicedetailsdto dto);
	
	public List<Devicedetailsdto> getalldevicedetails();
	
	public Devicedetailsdto getdetalsbyId(String id);
}
